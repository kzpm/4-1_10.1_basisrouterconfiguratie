!# R1
en
reload 
no

conf t
h R1

!# Beveilig de privileged-exec mode
enable secret cisco

!# Beveilig de user-exec mode
line console 0
password cisco
login

!# Beveilig toegang op afstand voor 5 sessies
line vty 0 4 
password cisco
login


!# Timeout wachten op user input
exec-timeout 5 30

!# Geef wachtwoorden versleuteld weer bij 'sh run'
service password-encryption

!# Voorkomen dat onjuiste commando's als zoekopdracht dienen
no ip domain-lookup

!# Banner maken met juridische impact
banner motd # Unauthorized access will be prosecuted #

end




